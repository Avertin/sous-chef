import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../models/recipe_model.dart';
// import '../../models/user_model.dart';
import '../screens/description.dart';
// import '../screens/desc.dart';
// import 'dart:convert';

class RecipesCarousel extends StatelessWidget {
  final List<Recipe> recipes;
  // final List<User> users;

  // RecipesCarousel(this.recipes, this.users);
  RecipesCarousel(this.recipes);

  PageController ctrl = PageController(viewportFraction: 0.8);
  
  @override
  Widget build(BuildContext context) {
    
    if(recipes.length > 0) {
      return Container(
        width: 375, 
        height: 490,
        color: Colors.transparent,
        child: ListView.builder(
          padding: EdgeInsets.only(top: 10, bottom: 10, left: 20),
          controller: ctrl,
          itemCount: recipes.length, 
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index){
            Recipe recipe = recipes[index];
            return GestureDetector( 
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => Description(recipe)));
              },
              child: Container(
              margin: EdgeInsets.only(right: 10), 
              width: 300, 
              decoration: BoxDecoration(
                boxShadow: [BoxShadow(
                  color: Color.fromRGBO(110, 102, 102, 0.5),
                  offset: Offset(0, 0),
                  blurRadius: 5)
                ],
                color: Colors.yellow,
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(image: NetworkImage(recipe.imageURL), fit: BoxFit.cover),
              ),
              child: Stack(children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      width: 300,
                      height: 100,
                      decoration: BoxDecoration(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight:Radius.circular(10) ),
                      color: Color.fromRGBO(0, 0, 0, 0.25)), 
                      child: Padding(
                        padding: const EdgeInsets.only(top: 10, left: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  width: 140,
                                  child: Text(recipe.titre,style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold,color: Colors.white), maxLines: 2),
                                ),
                                SizedBox(height: 10),
                                Row(
                                  children: <Widget>[
                                    Icon(Icons.location_on,color: Colors.white, size: 18,),
                                    SizedBox(width: 5),
                                    Text(recipe.origine,style: TextStyle(fontSize: 18,color: Colors.white),),
                                  ],
                                ),
                              ],
                            ),
                            GestureDetector(
                              onTap: () {
                                addToFavorites(recipe.key);
                              },
                              child: IconButton(
                                iconSize: 25,
                                color: Colors.yellow,
                                icon: Icon(Icons.star_border),
                                onPressed: () {
                                  return null;
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )
          ],),),);
        },),
      );
    }
    else {
      return Center(
        child: Text("Welcome. Your list is empty",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 20.0),
        )
      );
    }
  }

  void addToFavorites(String id) {
    
  }
}



