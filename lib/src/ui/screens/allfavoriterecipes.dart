import 'package:flutter/material.dart';
import '../../models/recipe_model.dart';
import 'description.dart';

class AllFavoriteRecipes extends StatelessWidget{
  final List<Recipe> recipes;

  AllFavoriteRecipes(this.recipes);

  @override 
  Widget build(BuildContext context) {
    double dw = MediaQuery.of(context).size.width;
    double dh = MediaQuery.of(context).size.height;
    
    return Scaffold(
      appBar: AppBar(
        title: Text('Recipes founded'),
      ),
      body: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: recipes.length,
        itemBuilder: (BuildContext context, int index) {
          Recipe recipe = recipes[index];
          return GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => Description(recipe)));
            },
            child: Stack(
              children: <Widget> [
                Container(
                  margin: EdgeInsets.fromLTRB(40.0, 5.0, 20.0, 5.0),
                  height: 190.0,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20.0),
                    boxShadow: [BoxShadow(
                      color: Color.fromRGBO(210, 202, 202, 0.5),
                      offset: Offset(0, 0),
                      blurRadius: 5)
                    ],
                  ),
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(100, 20, 20, 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget> [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: 110.0,
                              // padding: EdgeInsets.only(left: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(recipe.titre, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),maxLines: 2, overflow: TextOverflow.ellipsis,),
                                  SizedBox(height: 20),
                                  Text(recipe.origine, style: TextStyle(fontSize: 16),),
                                ],
                              ),
                            ),
                            IconButton(
                              iconSize: 25,
                              color: Colors.yellow,
                              icon: Icon(Icons.star_border),
                              onPressed: () {
                                return null;
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  left: 20.0,
                  top: 15.0,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20.0),
                    child: Image(
                      width: 110, 
                      image: NetworkImage(recipe.imageURL)
                    ),
                  ),
                ),
              ], 
            ),
          );
        },
      ),
    );
  }
}