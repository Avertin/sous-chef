import 'package:flutter/material.dart';
import 'package:souschef/src/ui/screens/allfavoriterecipes.dart';
import 'package:souschef/src/ui/screens/foundedRecipes.dart';
import 'filter_screen.dart';
import 'allrecipes.dart';
import '../widgets/recipes_carousel.dart';  
import '../widgets/favorite_recipes_caroussel.dart';  
import '../../services/authentication.dart';
import 'package:firebase_database/firebase_database.dart';
import '../../models/recipe_model.dart';
// import '../../models/user_model.dart';
import 'dart:async';

class BottomNavBar extends StatefulWidget {
  BottomNavBar({Key key, this.auth, this.userId, this.logoutCallback})
    : super(key: key);

  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;

  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      // print(_selectedIndex);
    });
  }

  Widget toShow() {
    switch (_selectedIndex) {
      case 0:
        return home();
        break;
      case 1:
      return profile();
      default:
      return null;
    }
  }

  List<Recipe> _recipeList;
  List<Recipe> _favoriteRecipesList = [];

  // final dbRef = FirebaseDatabase.instance.reference().child("user").orderByKey().equalTo().once();

  final FirebaseDatabase _database = FirebaseDatabase.instance;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  StreamSubscription<Event> _onRecipeAddedSubscription;
  StreamSubscription<Event> _onRecipeChangedSubscription;

  Query _recipeQuery;

  @override
  void initState() {
    super.initState();

    final dbRef = FirebaseDatabase.instance.reference().child("user").orderByKey().equalTo(widget.userId).once();
    // print(dbRef);

    _recipeList = new List();

    _recipeQuery = _database
      .reference()
      .child("recipe")
      .orderByChild("key");
    _onRecipeAddedSubscription = _recipeQuery.onChildAdded.listen(_onEntryAdded);
    _onRecipeChangedSubscription = _recipeQuery.onChildChanged.listen(_onEntryChanged);

  }

  _onEntryAdded(Event event) {
    setState(() {
      _recipeList.add(Recipe.fromSnapshot(event.snapshot));
    });
  }

  _onEntryChanged(Event event) {
    var oldEntry = _recipeList.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });

    setState(() {
      _recipeList[_recipeList.indexOf(oldEntry)] = Recipe.fromSnapshot(event.snapshot);
    });
  }

  @override
  void dispose() {
    _onRecipeAddedSubscription.cancel();
    _onRecipeChangedSubscription.cancel();
    super.dispose();
  }

  Widget home() {
    return SafeArea(
      child: Column(
        mainAxisAlignment:MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 10),
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  height: 45,
                  margin: EdgeInsets.only(left:15,top: 0,bottom: 0), 
                  decoration: BoxDecoration(
                    color: Colors.white, 
                    borderRadius: BorderRadius.circular(50),
                    boxShadow: [BoxShadow(
                      color: Color.fromRGBO(210, 202, 202, 0.5),
                      offset: Offset(0, 0),
                      blurRadius: 5)
                    ],  
                  ),
                  child: TextField(
                    onSubmitted: (keywordRecipe) {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => FoundedRecipes(recipeFinder(keywordRecipe, _recipeList))));
                    },
                    style: TextStyle(fontSize:16 ), 
                    decoration: InputDecoration(
                      border: InputBorder.none, 
                      hintText: 'Search recipes...',
                      hintStyle: TextStyle(color:Colors.grey, fontSize:16, fontWeight: FontWeight.w400),
                      prefixIcon: Icon(Icons.search,size: 20,color: Colors.green,)
                    ),
                  ),
                ),
              ),
              FlatButton( 
                onPressed: (){Navigator.push(
                  context, MaterialPageRoute(
                    builder: (context) => FilterScreen(_recipeList)
                  ),
                );},
                child: Container(
                  width: 45, 
                  height: 45,
                  decoration: BoxDecoration(color: Colors.green, borderRadius: BorderRadius.circular(10)),
                  child: Icon(Icons.filter_list, color: Colors.white)
                )
              ),
            ],
          ),
          SizedBox(height: 10),
          Padding(padding: EdgeInsets.only(left: 20, right:20, top: 10),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Recipe', style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600)),
                GestureDetector( 
                  child: Text('See all', style: TextStyle(color: Colors.green),),
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => AllRecipe(_recipeList)));
                  },
                )
              ],
            )
          ),
          SizedBox(height: 10),
          RecipesCarousel(_recipeList),
          // SizedBox(height: 10),
          // Padding(padding: EdgeInsets.only(left: 20, right:20, top: 10),
          //   child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //     children: <Widget>[
          //       Text('Favorite recipes', style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600)),
          //       GestureDetector( 
          //         child: Text('See all', style: TextStyle(color: Colors.green),),
          //         onTap: () {
          //           Navigator.push(context, MaterialPageRoute(builder: (context) => AllFavoriteRecipes(_recipeList)));
          //         },
          //       )
          //     ],
          //   )
          // ),
          // SizedBox(height: 10),
          // FavoriteRecipesCarousel(_favoriteRecipesList),
          // FavoriteRecipesCarousel(_recipeList),
        ],
      ),
    );
  }

  Widget profile() {
    return Container(
      child: Text(widget.userId),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        children: <Widget>[
          toShow(),
        ],
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          boxShadow: [BoxShadow(
            color: Color.fromRGBO(210, 202, 202, 0.5),
            offset: Offset(0, 0),
            blurRadius: 5)
          ]
        ),
        child: BottomNavigationBar(
          elevation: 0, 
          backgroundColor: Colors.white, 
          iconSize: 20,
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Home',style: TextStyle(fontWeight: FontWeight.bold),),),
            BottomNavigationBarItem(icon: Icon(Icons.person_outline,), title: Text('Profile',style: TextStyle(fontWeight: FontWeight.bold),),),
          ],

          currentIndex: _selectedIndex,
          selectedItemColor: Colors.green,
          onTap: _onItemTapped,
          
        )
      )
    );
  }  
}

recipeFinder(String keywordRecipe, List<Recipe> recipes) {
  List<Recipe> result = [];
  if(keywordRecipe == ''){
    result = [];
    return result;
  }
  else {
    recipes.forEach((r) {
      if(r.titre.toLowerCase().contains(keywordRecipe.toLowerCase())) {
        result.add(r);
      }
    });
    return result;
  }
  
}

  
  