import 'package:flutter/material.dart';
import '../../models/recipe_model.dart';

class Desc extends StatefulWidget{
  final Recipe recipe;

  Desc(this.recipe);

  @override
  _DescState createState() => _DescState();
}

class _DescState extends State<Desc>{
  List ings = [];
  List<Text> ingT = [];

  List recs = [];
  List<Text> recT = [];

  @override
  void initState() {
    super.initState();

    widget.recipe.ingredients.forEach((i) {
      if(i != null) {
        ings.add(i);
      }
    });

    widget.recipe.recette.forEach((r) {
      if(r != null) {
        recs.add(r);
      }
    });
  }

  @override
  void dispose() {
    ings = [];
    ingT = [];

    recs = [];
    recT = [];
    super.dispose();
  }

  List<Widget> showRec(List rec) {
    rec.forEach((r) {
      recT.add(Text(r, style: TextStyle(fontStyle: FontStyle.italic),));
    });
    return recT;
  }

  List<Widget> showIng(List ing) {
    ing.forEach((i) {
      ingT.add(Text(i, style: TextStyle(fontStyle: FontStyle.italic),));
    });
    return ingT;
  }
  
  @override 
  Widget build(BuildContext context) {
    double dw = MediaQuery.of(context).size.width;
    double dh = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                height: dw,
                width: dw,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      offset: Offset(0.0, 0.2),
                      blurRadius: 6.0,
                    ),
                  ],   
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(30.0),
                  child: Image(
                    image: NetworkImage(widget.recipe.imageURL),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 40.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      iconSize: 25,
                      color: Colors.white,
                      icon: Icon(Icons.chevron_left),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
          Expanded(
            child: ListView(
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 25.0),
                child: Text('Ingredients', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w400, decoration: TextDecoration.underline)),
              ),
              SizedBox(height: 8.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: showIng(ings).toList(),
                ),
              ),
              SizedBox(height: 20.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 25.0),
                child: Text('Recipe', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w400, decoration: TextDecoration.underline)),
              ),
              SizedBox(height: 8.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: showRec(recs).toList(),
                ),
              ),
            ],
          ),
          ),
        ],
      ),
    );
  }
}